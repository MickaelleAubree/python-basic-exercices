"""
Exercices.

Implémentez les méthodes ci-dessous.
Pour executer vos tests il vous faudra utiliser pytest
"""

def htc_to_ttc(htc_cost: float, discount_rate: float = 0) -> float :
    """
    Exercice 1 :
    Calcule le coût TTC d'un produit.
    discount_rate : taux de réduction compris entre 0 et 1
    Taux de taxes : 20.6 %
    Retourne un float arrondi à deux décimales
    """
    htc_cost = 10.0
    discount_rate = (0.5 * htc_cost) / 100
    ttc_cost = (htc_cost + htc_cost * 0.26) - discount_rate
    return float(round(ttc_cost, 2))

def divisors(value: int = 0):
    """
    Exercice 2 :
    A partir d'un nombre donné,
    retourne ses diviseurs (sans répétition)
    s’il y en a, ou « PREMIER » s’il n’y en a pas.
    """
     for n in range(1, value):
        if value % n == 0:
            if value % 1 == 0:
                print("PREMIER")
            else:
                print("Pas premier")


print(divisors(7))
